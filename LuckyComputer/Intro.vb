﻿Public Class Intro
    Private Sub Intro_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Left = -320
        Me.Top = My.Computer.Screen.Bounds.Height / 2 - 120
    End Sub
    Private Sub Intro_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Me_Move.Start()
        Me.Cursor = New Cursor(Main.CursorIntro)
        Me.Activate()
    End Sub
    Private Sub Me_Move_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me_Move.Tick
        If My.Computer.Screen.Bounds.Width / 2 - 160 <= Me.Left Then
            Me_Move.Stop()
            Lucky.ShowDialog()
        Else
            Me.Left += 10
        End If
    End Sub
End Class
