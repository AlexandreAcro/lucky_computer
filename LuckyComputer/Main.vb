﻿Imports System.Runtime.InteropServices
Public Class Main
    Public tostart As Short = 0, lc_files(0) As String, mouse_position As Point, selected_party(2) As Short, is_one_shard As Boolean = True, have_opened_party As Boolean = False
    <DllImport("user32.dll", EntryPoint:="LoadCursorFromFileW", CharSet:=CharSet.Unicode)> _
    Public Shared Function LoadCursorFromFile(ByVal str As [String]) As IntPtr
    End Function
    Public CursorIntro As IntPtr = IntPtr.Zero
    Public CursorLC As IntPtr = IntPtr.Zero
    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Left = -200
    End Sub
    Dim WshShell As Object, strDesktop As String, lnk_name() As String, settings_open As Boolean = False
    Private Sub Main_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        mouse_position.X = MousePosition.X
        mouse_position.Y = MousePosition.Y
        tostart = 0
        Cursor_Stay.Start()
        WshShell = CreateObject("WScript.Shell")
        strDesktop = WshShell.SpecialFolders("Startup")
        If My.Computer.FileSystem.FileExists(Application.StartupPath & "/Set.ini") = True Then
            lnk_name = IO.File.ReadAllLines(Application.StartupPath & "/Set.ini")
        Else
            lnk_name(0) = "Lucky Computer"
            lnk_name(1) = "LuckyComputer_CH"
            Try
                IO.File.WriteAllLines(Application.StartupPath & "/Set.ini", lnk_name)
            Catch
            End Try
        End If
        Dim way_to_cursors As String = Environ("Temp")
        My.Computer.FileSystem.WriteAllBytes(way_to_cursors & "\CursorLC_I.ani", My.Resources.CursorI, False)
        My.Computer.FileSystem.WriteAllBytes(way_to_cursors & "\CursorLC_L.ani", My.Resources.CursorLC, False)
        CursorIntro = LoadCursorFromFile(way_to_cursors & "\CursorLC_I.ani")
        CursorLC = LoadCursorFromFile(way_to_cursors & "\CursorLC_L.ani")
        My.Computer.FileSystem.DeleteFile(way_to_cursors & "\CursorLC_I.ani")
        My.Computer.FileSystem.DeleteFile(way_to_cursors & "\CursorLC_L.ani")
        Array.Resize(lc_files, 0)
        Try
            Dim files() As String = IO.Directory.GetFiles(Application.StartupPath, "*.wav", IO.SearchOption.TopDirectoryOnly)
            For i As Short = 0 To files.Length - 1
                If Mid(files(i), files(i).LastIndexOf("\") + 2).IndexOf("LC_") = 0 Then
                    Array.Resize(lc_files, lc_files.Length + 1)
                    lc_files(lc_files.Length - 1) = files(i)
                End If
            Next
        Catch
        End Try
        If My.Computer.FileSystem.FileExists(Application.StartupPath & "/cr.tmp") Then
            If My.Computer.FileSystem.ReadAllText(Application.StartupPath & "/cr.tmp") = "LC_res" Then
                is_one_shard = False
            End If
        End If
        If Process.GetProcessesByName(lnk_name(1)).Length = 0 Then
            Shell("RunDLL32.exe shell32.dll, ShellExec_RunDLL " & Application.StartupPath & "\" & lnk_name(1) & ".exe")
        End If
        Processor_Working.NextValue()
    End Sub
    Dim scan_launch_booster As Short = 30
    Private Sub Cursor_Stay_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cursor_Stay.Tick
        If scan_launch_booster = 30 Then
            scan_launch_booster = 0
            Try
                If My.Computer.FileSystem.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "/LC Settings.ls3") = "LC:Settings LC 142372813721546152973243252356+23960692369753030960390" And settings_open = False Then
                    My.Computer.FileSystem.DeleteFile(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "/LC Settings.ls3")
                    settings_open = True
                    Settings.ShowDialog()
                    settings_open = False
                    If selected_party(0) = 1 Then
                        mouse_position.X = MousePosition.X
                        mouse_position.Y = MousePosition.Y
                        tostart = 601
                        Intro.ShowDialog()
                    ElseIf selected_party(0) = 2 Then
                        mouse_position.X = MousePosition.X
                        mouse_position.Y = MousePosition.Y
                        tostart = 601
                        Party_shard.ShowDialog()
                    End If
                    selected_party(0) = 0
                End If
            Catch
            End Try
            My.Computer.FileSystem.WriteAllText(Application.StartupPath & "/cr.tmp", "LC_answer", False)
        Else
            scan_launch_booster += 1
        End If
        If tostart = 600 And mouse_position.X = MousePosition.X And mouse_position.Y = MousePosition.Y And settings_open = False Then
            Dim r As New Random
            If r.Next(4) = 1 And Int(Processor_Working.NextValue) < 85 Then
                tostart = 601
                Intro.ShowDialog()
            ElseIf (r.Next(4) = 2 Or r.Next(4) = 3) And (Int(Processor_Working.NextValue) < 95) Then
                tostart = 601
                selected_party(1) = -1
                Party_shard.ShowDialog()
            Else
                tostart = 300
            End If
        ElseIf (tostart = 300 Or tostart = 450) And (mouse_position.X = MousePosition.X And mouse_position.Y = MousePosition.Y And settings_open = False) Then
            Dim r As New Random
            If r.Next(4) = 1 And Int(Processor_Working.NextValue) < 95 Then
                tostart = 601
                selected_party(1) = -1
                Party_shard.ShowDialog()
            Else
                If tostart = 300 Then
                    tostart = 301
                Else
                    tostart = 451
                End If
            End If
        ElseIf mouse_position.X = MousePosition.X And mouse_position.Y = MousePosition.Y And settings_open = False Then
            If tostart < 600 Then
                tostart += 1
            End If
        Else
            tostart = 0
            mouse_position.X = MousePosition.X
            mouse_position.Y = MousePosition.Y
            My.Computer.Audio.Stop()
            Intro.Close()
            Lucky.Close()
            Party_shard.Close()
            Intro.Me_Move.Stop()
            Lucky.Main_Timer.Stop()
            Party_shard.Off_timer.Stop()
            If have_opened_party = True Then
                Dim r As New Random
                If r.Next(50) = 21 Then
                    tostart = 601
                    selected_party(1) = -1
                    Party_shard.ShowDialog()
                End If
            End If
            have_opened_party = False
        End If
        If scan_launch_booster = 10 Then
            Try
                If My.Computer.FileSystem.FileExists(strDesktop & "/" & lnk_name(0) & ".lnk") = False Then
                    Dim oShellLink As Object
                    oShellLink = WshShell.CreateShortcut(strDesktop & "\" & lnk_name(0) & ".lnk") 'Имя ярлыка
                    oShellLink.TargetPath = Application.StartupPath & "\" & lnk_name(0) & ".exe" 'Путь к объекту
                    oShellLink.WindowStyle = 1 'Стиль окна
                    oShellLink.IconLocation = Application.StartupPath & "\" & lnk_name(0) & ".exe,0" 'Вид значка
                    oShellLink.Description = lnk_name(0) 'Комментарий к ярлыку
                    oShellLink.WorkingDirectory = strDesktop 'Имя рабочего каталога
                    oShellLink.Save() 'Создать
                End If
            Catch
            End Try
        End If
    End Sub
End Class
