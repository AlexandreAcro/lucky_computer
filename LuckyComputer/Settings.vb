﻿Public Class Settings
    Private Sub Settings_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        If Main.lc_files.Length > 0 Then
            RadioButton2.Checked = True
            CheckBox1.Enabled = True
            ComboBox1.Items.Clear()
            For i As Short = 0 To Main.lc_files.Length - 1
                ComboBox1.Items.Add(Mid(Main.lc_files(i), Main.lc_files(i).LastIndexOf("\") + 2, Main.lc_files(i).LastIndexOf(".") - Main.lc_files(i).LastIndexOf("\") - 1))
            Next
            ComboBox1.SelectedIndex = 0
        Else
            RadioButton1.Checked = True
            RadioButton2.Enabled = False
            CheckBox1.Enabled = False
            ComboBox1.Enabled = False
        End If
        ComboBox2.SelectedIndex = 0
        ComboBox1.Enabled = False
        CheckBox1.Checked = True
        seconds = 0
    End Sub
    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        ComboBox1.Enabled = False
        CheckBox1.Enabled = False
    End Sub
    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        If CheckBox1.Checked Then
            ComboBox1.Enabled = False
        Else
            ComboBox1.Enabled = True
        End If
        CheckBox1.Enabled = True
    End Sub
    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked Then
            ComboBox1.Enabled = False
        Else
            ComboBox1.Enabled = True
        End If
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If ComboBox2.SelectedIndex = 0 Then
            If RadioButton1.Checked = True Then
                Main.selected_party(0) = 1
            Else
                Main.selected_party(0) = 2
                If CheckBox1.Checked Then
                    Main.selected_party(1) = -1
                Else
                    Main.selected_party(1) = ComboBox1.SelectedIndex
                End If
            End If
            Me.Close()
        Else
            If RadioButton1.Checked = True Then
                Main.selected_party(0) = 1
            Else
                Main.selected_party(0) = 2
                If CheckBox1.Checked Then
                    Main.selected_party(1) = -1
                Else
                    Main.selected_party(1) = ComboBox1.SelectedIndex
                End If
            End If
            Select Case ComboBox2.SelectedIndex
                Case 1 : seconds = 5
                Case 2 : seconds = 10
                Case 3 : seconds = 30
                Case 4 : seconds = 60
                Case 5 : seconds = 300
            End Select
            Party_start.Start()
            Me.Left = -220
        End If
    End Sub
    Dim seconds As Short = 0
    Private Sub Party_start_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Party_start.Tick
        If seconds = 0 Then
            Party_start.Stop()
            Me.Close()
        Else
            seconds -= 1
        End If
    End Sub
End Class
