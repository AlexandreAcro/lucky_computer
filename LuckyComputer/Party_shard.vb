﻿Public Class Party_shard
    Dim way As String = Application.StartupPath, off_time As Short
    Private Sub Party_shard_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim r As New Random
        If Main.selected_party(1) = -1 Then
            If Main.is_one_shard = True Then
                If My.Computer.FileSystem.FileExists(way & "/LCB_I.gif") Then
                    PictureBox1.ImageLocation = way & "/LCB_I.gif"
                End If
                Off_timer.Start()
            Else
                Dim r_1 As Short = r.Next(7)
                If r_1 = 0 Then
                    If My.Computer.FileSystem.FileExists(way & "/LCB_1.gif") Then
                        PictureBox1.ImageLocation = way & "/LCB_1.gif"
                    End If
                ElseIf r_1 = 1 Then
                    If My.Computer.FileSystem.FileExists(way & "/LCB_2.gif") Then
                        PictureBox1.ImageLocation = way & "/LCB_2.gif"
                    End If
                ElseIf r_1 = 2 Then
                    If My.Computer.FileSystem.FileExists(way & "/LCB_3.gif") Then
                        PictureBox1.ImageLocation = way & "/LCB_3.gif"
                    End If
                ElseIf r_1 = 3 Then
                    If My.Computer.FileSystem.FileExists(way & "/LCB_4.gif") Then
                        PictureBox1.ImageLocation = way & "/LCB_4.gif"
                    End If
                ElseIf r_1 = 4 Then
                    If My.Computer.FileSystem.FileExists(way & "/LCB_5.gif") Then
                        PictureBox1.ImageLocation = way & "/LCB_5.gif"
                    End If
                ElseIf r_1 = 5 Then
                    If My.Computer.FileSystem.FileExists(way & "/LCB_6.gif") Then
                        PictureBox1.ImageLocation = way & "/LCB_6.gif"
                    End If
                ElseIf r_1 = 6 Then
                    If My.Computer.FileSystem.FileExists(way & "/LCB_7.gif") Then
                        PictureBox1.ImageLocation = way & "/LCB_7.gif"
                    End If
                End If
                Random_gen.Start()
            End If
        Else
            Dim r_1 As Short = r.Next(7)
            If r_1 = 0 Then
                If My.Computer.FileSystem.FileExists(way & "/LCB_1.gif") Then
                    PictureBox1.ImageLocation = way & "/LCB_1.gif"
                End If
            ElseIf r_1 = 1 Then
                If My.Computer.FileSystem.FileExists(way & "/LCB_2.gif") Then
                    PictureBox1.ImageLocation = way & "/LCB_2.gif"
                End If
            ElseIf r_1 = 2 Then
                If My.Computer.FileSystem.FileExists(way & "/LCB_3.gif") Then
                    PictureBox1.ImageLocation = way & "/LCB_3.gif"
                End If
            ElseIf r_1 = 3 Then
                If My.Computer.FileSystem.FileExists(way & "/LCB_4.gif") Then
                    PictureBox1.ImageLocation = way & "/LCB_4.gif"
                End If
            ElseIf r_1 = 4 Then
                If My.Computer.FileSystem.FileExists(way & "/LCB_5.gif") Then
                    PictureBox1.ImageLocation = way & "/LCB_5.gif"
                End If
            ElseIf r_1 = 5 Then
                If My.Computer.FileSystem.FileExists(way & "/LCB_6.gif") Then
                    PictureBox1.ImageLocation = way & "/LCB_6.gif"
                End If
            ElseIf r_1 = 6 Then
                If My.Computer.FileSystem.FileExists(way & "/LCB_7.gif") Then
                    PictureBox1.ImageLocation = way & "/LCB_7.gif"
                End If
            End If
        End If
        If Main.is_one_shard = True And Main.selected_party(1) = -1 Then
            off_time = 20
        Else
            off_time = 5
        End If
        Main.is_one_shard = False
    End Sub
    Private Sub Party_shard_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        If Main.selected_party(1) <> -1 Then
            My.Computer.Audio.Play(Main.lc_files(Main.selected_party(1)))
            Off_timer.Start()
        End If
    End Sub
    Private Sub Random_gen_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Random_gen.Tick
        Dim r As New Random
        My.Computer.Audio.Play(Main.lc_files(r.Next(Main.lc_files.Length)))
        Random_gen.Stop()
        Off_timer.Start()
    End Sub
    Private Sub Off_timer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Off_timer.Tick
        If off_time = 0 Then
            Off_timer.Stop()
            Main.tostart = 0
            Main.have_opened_party = True
            Me.Close()
        Else
            off_time -= 1
        End If
    End Sub
End Class
